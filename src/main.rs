use std::fs::read_to_string;

fn main() {
    let file = read_to_string("input.txt").expect("Should be able to read file.");
    let mut count = 0;

    for line in file.lines() {
        let split_line_vec: Vec<&str> = line.split(",").collect();
        if let [first_range, second_range] = &split_line_vec[..] {
            let first_split_range_vec: Vec<&str> = first_range.split("-").collect();
            if let [first_range_start, first_range_end] = &first_split_range_vec[..] {
                let second_split_range_vec: Vec<&str> = second_range.split("-").collect();
                if let [second_range_start, second_range_end] = &second_split_range_vec[..] {
                    let first_range_start_number: i32 = first_range_start.parse().unwrap();
                    let first_range_end_number: i32 = first_range_end.parse().unwrap();
                    let second_range_start_number: i32 = second_range_start.parse().unwrap();
                    let second_range_end_number: i32 = second_range_end.parse().unwrap();
                    if (first_range_start_number >= second_range_start_number
                        && first_range_start_number <= second_range_end_number)
                        || (first_range_end_number <= second_range_end_number
                            && first_range_end_number >= second_range_start_number)
                        || (second_range_start_number >= first_range_start_number
                            && second_range_start_number <= first_range_end_number)
                        || (second_range_end_number <= first_range_end_number
                            && second_range_end_number >= first_range_start_number)
                    {
                        count += 1;
                    }
                }
            }
        };
    }
    println!("Total Overlapping Pairs: {}", count);
}
